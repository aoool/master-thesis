%!TEX program = xelatex
\documentclass[10pt, compress]{beamer}
\usetheme[titleprogressbar]{m}

\usepackage{booktabs}
\usepackage[scale=2]{ccicons}
\usepackage{minted}
\usepackage{array}
\usepackage{../thesis/tikz-uml}  % UML diagrams
\usepackage{color}
\usepackage{pgfplots} % graphs
\usepackage{pgfplotstable}

\usepackage{array}
\newcolumntype{C}[1]{>{\centering\arraybackslash}m{#1}}
\newcolumntype{R}[1]{>{\raggedleft\arraybackslash}m{#1}}

\usepgfplotslibrary{dateplot}

\usemintedstyle{trac}

\title{\huge Tiered File System: \newline
           Optimization of Architecture and Management Algorithms}
\subtitle{Master’s Thesis Presentation}
\author{\footnotesize 
        \underline{\textsc{Author:}}   \newline   {\small Sergey Morozov}  
        \newline \newline
        \underline{\textsc{Scientific supervisor:}}    \newline 
        {\scriptsize Dr. Sc. (Phys.-Math.), Professor} 
        {\small Vyacheslav Nesterov} \newline \newline
        \underline{\textsc{Reviewer:}} \newline
        {\small Andrey Pakhomov}, 
        {\scriptsize Senior Solutions Manager at Dell EMC} \newline }
\institute{Mathematics and Mechanics Faculty, St. Petersburg State University}

\begin{document}

\maketitle

\begin{frame}[fragile]
\frametitle{Introduction}
    \large
    \begin{itemize}
        \item Enterprise Storage
        \begin{itemize}
            \item High Performance
            \item Limited Capacity
            \item Expensive
        \end{itemize}
        
        \item Cloud Object Storage
        \begin{itemize}
            \item Low Performance
            \item Limitless Capacity
            \item Cheap
        \end{itemize}
        
        \item Automated Storage Tiering
        \begin{itemize}
            \item Smart Data Migration Policies
            \item ``Hot'' Data on High-Performance Tier (Expensive)
            \item ``Cold'' Data on Low-Performance Tier (Cheap)
        \end{itemize}
    \end{itemize}
\end{frame}




\begin{frame}[fragile]
    \frametitle{Problem Statement}
     Design and implement a file system-agnostic policy-based 
     software component responsible for data synchronization
     between a POSIX-conformant file system and cloud object storage.

    \begin{itemize}
        \small
        \item Investigate automated storage tiering problems in an environment 
          that includes a distributed file system and cloud object storage.
          \newline
        \item Extract and compare important features of modern distributed file 
          systems from the perspective of automated storage tiering.
          \newline
        \item Design a software component that enables automated storage tiering 
          between a POSIX-conformant file system and cloud object storage.
          \newline
        \item Implement the designed software component and 
          evaluate its performance.
    \end{itemize}
\end{frame}




\begin{frame}[fragile]
    \frametitle{Automated Storage Tiering Problems}
    \large 
    \begin{itemize}
        \item Weak consistency model of cloud object storage
        \item Partitioning
        \item Data access pattern for a general case
        \item Metadata size
        \item Graphical file managers
    \end{itemize}
\end{frame}




\begin{frame}[fragile]
    \frametitle{Comparison of Some Distributed File Systems}
    \small
    \renewcommand{\arraystretch}{2}
    \begin{center}
\begin{tabular}{|c|c|c|c|c|}
\hline

& \textbf{MooseFS} & \textbf{CephFS} & \textbf{GlusterFS} & \textbf{OrangeFS} \\ 

\hline

\textbf{POSIX Conformance} & full       & near      & full         & near     \\

\hline

\textbf{Tiering Support}   & yes   &    yes      &    yes         &    no     \\

\hline

\textbf{Fault Tolerance}   &    yes   & yes   &    yes         &    yes     \\

\hline

\textbf{License}           & GPLv2      & LGPLv2.1  & GPLv2/LGPLv3 & LGPLv2.1 \\

\hline
\end{tabular}
    \end{center}
\end{frame}

\begin{frame}[fragile]
    \frametitle{Design: Component Diagram}
    \begin{figure}[ht]
        \centering
        \tikzumlset{font=\large}
        \scalebox{0.48}{\input{../figures/component-diagram-pres.tex}}
    \end{figure}
\end{frame}

\begin{frame}[fragile]
    \frametitle{Design: Deployment Diagram}
    \begin{figure}[ht]
        \centering
        \scalebox{0.65}{\input{../figures/deployment-diagram-pres.tex}}
    \end{figure}
\end{frame}



\begin{frame}[fragile]
    \frametitle{Implementation: Overview}
    \renewcommand{\arraystretch}{1.5}

        \begin{tabular}{ll}
    \textsc{Source Code:}   & \url{https://github.com/aoool/CloudTieringFS} \\
    \textsc{Language:}      & C \\
    \textsc{Libraries:}     & libs3, dotconf \\
    \textsc{License:}       & GNU General Public License v3.0
        \end{tabular}


\end{frame}


\begin{frame}[fragile]
    \frametitle{Implementation: Performance}
    \large
    \begin{itemize}
        \item Compute nodes---Amazon~EC2
        \begin{itemize}
            \item SUSE Linux Enterprise Server 12 SP2
            \item t2.micro (1 vCPU, 1 GiB RAM, 30 GB of Elastic Block Storage)
        \end{itemize}
        \item Cloud object storage---Amazon~S3
        \item Region---North~Virginia
        \item Configurations
        \begin{itemize}
            \item Single node---BtrFS
            \item Multiple nodes---OrangeFS
        \end{itemize}
    \end{itemize}
\end{frame}



\begin{frame}[fragile]
    \frametitle{Implementation: Performance (BtrFS)}
    \begin{figure}[h!]
         \centering
         \scalebox{1.0}{\input{../figures/btrfs-bar-plot-pres}}
    \end{figure}
\end{frame}


\begin{frame}[fragile]
    \frametitle{Implementation: Performance (BtrFS)}
    \begin{figure}[h!]
         \centering
         \scalebox{1.0}{\input{../figures/btrfs-scatter-plot-16mb-pres}}
    \end{figure}
\end{frame}


\begin{frame}[fragile]
    \frametitle{Implementation: Performance (OrangeFS)}
     \begin{figure}[h!]
         \centering
         \scalebox{1.0}{\input{../figures/orangefs-bar-plot-pres}}
    \end{figure}
\end{frame}


\begin{frame}[fragile]
    \frametitle{Implementation: Performance (OrangeFS)}
    \begin{figure}[h!]
         \centering
         \scalebox{1.0}{\input{../figures/orangefs-scatter-plot-16mb-pres}}
    \end{figure}
\end{frame}



\begin{frame}[fragile]
    \frametitle{Results}
     Designed and implemented a file system-agnostic policy-based 
     software component responsible for data synchronization
     between a POSIX-conformant file system and cloud object storage.
    \begin{itemize}
        \small
        \item Problems of automated storage tiering in 
          an environment that includes a distributed 
          file system and cloud object storage were investigated.
          \newline
          \textit{\color{gray} [ weak consistency, partitioning, metadata size, 
                    graphical file managers ]}
        \item Important features of modern distributed file systems 
          related to the automated storage tiering were identified and compared.
          \newline
          \textit{\color{gray} [ MooseFS, CephFS, GlusterFS, OrangeFS ]}
          %\newline
        \item A software component enabling automated storage tiering 
          between a POSIX-conformant file system
          and cloud object storage was designed. 
          \newline
          \textit{\color{gray} [ file system-agnostic, distributed ]}
          %\newline
        \item The designed software component was implemented and its 
          performance evaluated in single- and multi-node configurations. 
          \newline
          \textit{\color{gray} [ BtrFS, OrangeFS ]}   
    \end{itemize}
\end{frame}

%\plain{Appendix}

\end{document}